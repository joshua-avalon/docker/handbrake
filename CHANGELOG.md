# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## 1.0.0

### Added

- Initial Release

[unreleased]: https://gitlab.com/joshua-avalon/docker/handbrake/compare/1.0.0...master
