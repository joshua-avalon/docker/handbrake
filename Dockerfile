FROM ubuntu:18.04

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

# Add locale to display unicode characters
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y locales && \
    rm -rf /var/lib/apt/lists/* && \
    sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
    dpkg-reconfigure --frontend=noninteractive locales && \
    update-locale LANG=en_US.UTF-8

COPY root/ /

RUN chmod a+x /docker-entrypoint.sh

RUN apt-get update && \
    apt-get install -y software-properties-common && \
    add-apt-repository ppa:stebbins/handbrake-releases && \
    apt-get purge --auto-remove -y software-properties-common && \
    apt-get update && \
    apt-get install -y \
      fontconfig \
      handbrake-cli && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /files

ENTRYPOINT ["/docker-entrypoint.sh"]
