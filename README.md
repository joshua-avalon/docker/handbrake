# [HandBrake][project]

[![License][license_md]][license]
[![GitLab CI][gitlab_ci]][gitlab]
[![Docker Pull][docker_pull]][docker]
[![Docker Star][docker_star]][docker]
[![Docker Size][docker_size]][docker]
[![Docker Layer][docker_layer]][docker]

[HandBrake][handbrake] in Ubuntu.

## Usage

```bash
docker run -v "$(pwd):/files" -v "/usr/local/share/fonts:/usr/local/share/fonts:ro" --rm -d joshava/handbrake HandBrakeCLI --help
```

Note that this Docker image will rebuild font cache on start.

### Volume

- `/files` - Default work directories
- `/usr/local/share/fonts` - Fonts directories

[docker]: https://hub.docker.com/r/joshava/handbrake
[docker_pull]: https://img.shields.io/docker/pulls/joshava/handbrake.svg
[docker_star]: https://img.shields.io/docker/stars/joshava/handbrake.svg
[docker_size]: https://img.shields.io/microbadger/image-size/joshava/handbrake.svg
[docker_layer]: https://img.shields.io/microbadger/layers/joshava/handbrake.svg
[license]: https://gitlab.com/joshua-avalon/docker/handbrake/blob/master/LICENSE
[license_md]: https://img.shields.io/badge/license-Apache--2.0-green.svg
[gitlab]: https://gitlab.com/joshua-avalon/docker/handbrake/pipelines
[gitlab_ci]: https://gitlab.com/joshua-avalon/docker/handbrake/badges/master/pipeline.svg
[handbrake]: https://github.com/HandBrake/HandBrake
[project]: https://gitlab.com/joshua-avalon/docker/handbrake
