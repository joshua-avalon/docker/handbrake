#!/bin/bash

set -e

# Rebuild font cache
fc-cache -fv

exec "$@"
